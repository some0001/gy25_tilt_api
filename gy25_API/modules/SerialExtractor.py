from threading import Thread
from time import sleep, time
from queue import Queue
from serial import Serial
from gy25_helper import int_to_bytes, bytearr_to_hex_str
from time import time_ns

class Timer:
    def __init__(self):
        self.startTime = time()

    def time_elapsed(self):
        return time() - self.startTime

class SerialExtractor:
    '''
    Used for raw data extraction from gy25 via UART communication

    (init) Args:
        serPort           (str): Name of the serial device
        serBaud           (int): Baudrate for serial communication
        mode              (str): Specifies gy25 communication mode (Query or Auto)
        outputFilepath    (str): Filepath to file to which the raw data should be exported 
        bytesToRead       (int): Specifies how many bytes to read in single serial read 
        timeBetweenReads  (int): Specifies waiting time between serial read
    '''

    def __init__(self, serPort, serBaud, mode, outputFilepath, bytesToRead, timeBetweenReads):

        self.ser = Serial(serPort, serBaud)
        self.mode = mode
        self.outputFilepath = outputFilepath
        self.bytesToRead = bytesToRead
        self.timeBetweenReads = timeBetweenReads
        self.__startTime = time_ns()
        self.__queue = Queue()
        self.__wrFlag = False

    def ser_flush_input(self):
        self.ser.read(self.ser.in_waiting)

    def send_hex(self, *argv):
        for arg in argv:
            self.ser.write(bytearray.fromhex(arg))

    def read_and_write(self, timeToRun):
        self.__wrFlag = True
        wrThread = Thread(target=self.write_queue_to_file, daemon=True,)
        wrThread.start()

        if(self.mode == 'Auto'):
            self.read_serial_auto(timeToRun)
        elif(self.mode == 'Query'):
            self.read_serial_query(timeToRun)

        self.__wrFlag = False
        wrThread.join()

    def write_queue_to_file(self):
        wrBuffer = None

        with open(self.outputFilepath, 'w+') as f:
            while self.__wrFlag is True:
                if self.__queue.empty() is False:
                    wrBuffer = self.__queue.get()
                    if wrBuffer.decode('ascii')[0] == "#":
                        timestamp = ((time_ns() - self.__startTime) // 1000) % 4284000000000
                        f.write(str(timestamp))
                        f.write('\n')
                        f.write(wrBuffer.decode('ascii'))
                    sleep(0.1)

            while self.__queue.empty() is False:
                wrBuffer = self.__queue.get()
                if wrBuffer.decode('ascii')[0] == "#":
                    timestamp = ((time_ns() - self.__startTime) // 1000) % 4284000000000
                    f.write(str(timestamp))
                    f.write('\n')
                    f.write(wrBuffer.decode('ascii'))

    def read_serial_query(self, timeToRun):

        try:
            self.send_hex('A5', '54')
            self.ser_flush_input()

            timer = Timer()

            while timer.time_elapsed() < timeToRun:
                self.send_hex('A5', '51')
                self.__queue.put(self.ser.read(self.bytesToRead))
                sleep(self.timeBetweenReads)

        except KeyboardInterrupt:
            ('Serial reading interrupted, read count left: {}'.format(timeToRun))


    def read_serial_auto(self, timeToRun):

        rxBuffer = None
        self.send_hex('A5', '54')
        self.send_hex('A5', '55')
        self.send_hex('A5', '53')
        self.ser_flush_input()

        timer = Timer()

        while(self.ser.read(2) != b'\r\n'):
            pass

        try:
            while timer.time_elapsed() < timeToRun:
                while(self.ser.in_waiting < self.bytesToRead):
                    pass
                self.__queue.put(self.ser.read(self.bytesToRead))

        except KeyboardInterrupt:
            ('Serial reading interrupted')

