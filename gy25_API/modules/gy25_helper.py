import binascii

def bytearr_to_hex_str(bytearr):
    hexStr = binascii.hexlify(bytearr).decode('ascii')
    return hexStr

def int_to_bytes(n, minlen=0):
    if n > 0:
        arr = []
        while n:
            n, rem = n >> 8, n & 0xff
            arr.append(rem)
        b = bytearray(reversed(arr))
    elif n == 0:
        b = bytearray(b'\x00')
    else:
        raise ValueError('Only non-negative values supported')

    if minlen > 0 and len(b) < minlen: 
        b = (minlen-len(b)) * '\x00' + b
    return b

