from threading import Thread
from queue import Queue
from gy25_helper import bytearr_to_hex_str
from time import sleep

def str_to_float(strFloat):
    if(strFloat[0] == '+'):
        return float(strFloat[1:])
    elif(strFloat[0] == '-'):
        return - float(strFloat[1:])
    

class gy25_Parser:
    '''
    Parses and exports gy25 raw data into human readable format

    (init) Args:
        rawDataFilename (str): Filepath to gy25 raw data file
        outFilename     (str): Filepath to file to which the parsed data should be exported
    ''' 

    def __init__(self, rawDataFilename, outFilename):
        self.rawDataFilename = rawDataFilename
        self.outFilename = outFilename
        self.__strBuffer = ''
        self.__wrFlag = False
        self.__queue = Queue()

    def read_rawData(self):
        readSize = 64

        with open(self.rawDataFilename, 'r') as f:
            while(True):
                data = f.read(readSize)

                if len(data) is 0:
                    break

                self.__queue.put(data)

    def read_raw_parse_write(self):
        wrThread = Thread(target = self.parse_and_write, daemon=True,)
        self.__wrFlag = True
        wrThread.start()
        self.read_rawData()
        self.__wrFlag = False
        wrThread.join()

    def get_tail_idx(self):
        tail = None
        for i in range(4, len(self.__strBuffer)):
            if(self.__strBuffer[i] == '\n'):
                tail = i
                break
        return tail

    def parse_and_write(self):

        tempStr = None
        parsedCount = 0

        with open(self.outFilename, 'w') as f:
            while self.__wrFlag is True or self.__queue.empty() is False or len(self.__strBuffer) > 0:
                timestamp = None
                tempStr = self.__strBuffer

                if self.__queue.empty() is False and len(self.__strBuffer) < 128:
                    self.__strBuffer += self.__queue.get()

                tail = self.get_tail_idx()
                    
                if tail is not None:
                    timestamp = int(self.__strBuffer[:tail])
                    self.__strBuffer = self.__strBuffer[tail:]

                tail = self.get_tail_idx()
                if tail in [None, 1]:
                    self.__strBuffer = self.__strBuffer[1:]
                    tail = None
                    
                if tail:
                    data = self.__strBuffer[:tail]
                    header = data[:6]
                    data = data[6:]
                    if header == '\n#YPR=':
                        data = data.split(',')
                        yaw = str_to_float(data[0])
                        pitch = str_to_float(data[1])
                        roll = str_to_float(data[2])

                        data = [timestamp, roll, pitch, yaw]
                        f.flush()

                        for entry in data:
                            f.write(str(entry) + ' ')
                        f.write('\n')

                        parsedCount += 1
                        self.__strBuffer = self.__strBuffer[tail:]
                    else:
                        print('Parse error, skipping', self.__strBuffer[:tail], len(self.__strBuffer[:tail]))
                        self.__strBuffer = self.__strBuffer[1:]
                        pass

            print('Entries parsed: {}'.format(parsedCount))

