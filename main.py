from datetime import datetime
import os, sys
sys.path.append(os.getcwd() + '/gy25_API')
sys.path.append(os.getcwd() + '/gy25_API/modules')
from gy25_API import gy25_API
from matlab_export import export_to_matlab

def serial_menu():
    devList = ['/dev/ttyUSB0', '/dev/ttyS0', '/dev/ttyS1', '/dev/S1']
    print('Select serial device')
    print('1. {}'.format(devList[0]))
    print('2. {}'.format(devList[1]))
    print('3. {}'.format(devList[2]))
    print('4. {}'.format(devList[3]))

    dev = input()
    while dev not in ['1','2','3','4']:
        print('Invalid input entered')
        dev = input()
    dev = devList[int(dev)-1]

    baudList = [9600, 115200, 921600]
    print('Select serial baud rate')
    print('1. {}'.format(baudList[0]))
    print('2. {}'.format(baudList[1]))
    print('3. {}'.format(baudList[2]))

    baud = input()
    while baud not in ['1','2','3']:
        print('Invalid input entered')
        baud = input()
    baud = baudList[int(baud)-1] 

    return [dev, baud]
    
def mode_menu():
    modeList = ['Auto', 'Query']
    print('Select serial reading mode')
    print('1. {}'.format(modeList[0]))
    print('2. {}'.format(modeList[1]))

    mode = input()
    while mode not in ['1','2']:
        print('Invalid input entered')
        mode = input()
    mode = modeList[int(mode)-1]

    return mode

def gy25_extract_from_raw_export_matl(readCount, dataFilepath, dev, baud, mode):

    rawDataFilename = dataFilepath + 'raw_data'
    AP = gy25_API(rawDataFilename, dataFilepath)

    t1 = datetime.now()
    print('\nSerial reading initialized at {}'.format(t1.strftime('%H:%M:%S')))
    print('Saving output to {}'.format(rawDataFilename))
    AP.serial_read_and_write(dev, baud, mode, readCount)
    print('Reading from serial finished')
    print('Time taken: {}\n'.format(datetime.now() - t1))

    t2 = datetime.now()
    print("Extracting data and writing it to file....")
    AP.parse_and_write()
    print('Time taken: {}\n'.format(datetime.now() - t2))

    t3 = datetime.now()
    print('Exporting data to matlab')
    AP.parsed_file_to_matlab()
    print('Time taken: {}\n'.format(datetime.now() - t3))

    print('Finished, output saved to {}'.format(dataFilepath))
    print('\nTotal time taken: {}\n'.format(datetime.now() - t1))

def main():

    dev, baud = serial_menu()
    mode = 'Auto'

    print('Specify serial read time in seconds: ', end='')
    readCount = input()
    while(readCount.isdigit() is False):
        readCount = input()
    readCount = int(readCount)

    if os.path.isdir('./data') is False:
        os.mkdir('./data')
    dataDirectory = './data/{}'.format(datetime.now().strftime("%d.%m.%Y_%H%M%S"))
    os.mkdir(dataDirectory)
    dataFilepath = dataDirectory + '/'

    gy25_extract_from_raw_export_matl(readCount, dataFilepath, dev, baud, mode)


if __name__ == '__main__':
    main()


